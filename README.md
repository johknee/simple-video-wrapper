Simple Video API Wrapper
==========================

Include the vimeowrapper.js and then just call it like below. If you do not include a player_id query in your vimeo embed, it will create one for you. But you do need to have the *api=1* query in the src.

This wrapper also now works for youtube videos AND html5 video tags too, so the same simple API can be used for vimeo, youtube and vanilla html videos on the same page

```
<iframe src="//player.vimeo.com/video/87701971?title=0&byline=0&portrait=0&badge=0&api=1" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
```

```
var player = new vimeoWrapper($("iframe"),{
	ready 	: function(){ // When player is ready

	},
	playing	: function(object){ // While playing

	},
	loading	: function(object){ // While loading

	},
	finish	: function(object){ // When finished

	},
	seek	: function(object){ // When seeking is triggered

	},
	change	: function(object){

		switch(object.state)
		{
			case "play": // State is changed to play

				break;
			case "pause": // State is changed to pause

				break;
		}
	},
	seek_selector : "#sel" // Element to tie to seeking
});
```

When this is all set, you can interact with the player easily by just using this simple API

- player.play()
- player.pause()
- player.unload()
- player.loop(bool)
- player.seek(seconds)
- player.setVolume(0-100)
- player.colorChange(hexCode) - This is without the #