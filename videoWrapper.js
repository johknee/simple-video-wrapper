
function videoWrapper(el,prop)
{
	this.element		= el;
	this.id 			= "";

	if(this.element.get(0).tagName=="VIDEO")
	{
		this.src		= this.element.find("source:eq(0)").attr("src");
	}
	else
	{
		this.src		= this.element.attr('src');
	}
	this.url 			= this.src;
	this.query 			= "";

	if(this._hasParams)
	{
		if(this.src.split('?').length>1)
		{
			this.url		= window.location.protocol + this.src.split('?')[0];
			this.query		= this._parseQuery(this.src.split('?')[1]);
		}
	}

	this.nativeUrl 		= "";
	this.duration 		= 0;
	this.timeDuration	= "";
	this.state 			= "";
	this.color 			= "";
	this.nativeWidth 	= 0;
	this.nativeHeight 	= 0;
	this.volume 		= 0;
	this.source 		= this._determineSource(this.url);
	this.ytPlayer 		= "";
	this.ytTimeout 		= setTimeout(function(){ },0);
	this.loopVar		= false;


	this.progress 		= {
		percentage : 0
	};
	this.loadProgress 		= {
		percentage : 0
	};

	this.properties	= {
		ready 			: function(){},
		playing			: function(){},
		loading			: function(){},
		finish			: function(){},
		change			: function(){},
		seek			: function(){},
		seek_selector 	: false
	}

	$.extend(this.properties,prop);

	this._addListeners();
};

videoWrapper.prototype._hasParams = function(src)
{
	if(this.src.indexOf("?")>=0)
	{
		return true;
	}
	else
	{
		return false;
	}
};

videoWrapper.prototype._determineSource = function(str)
{
	if(this.element.get(0).tagName=="VIDEO")
	{
		this._setVideoInfo();
		return "html";
	}
	if(str.search("vimeo")>=0)
	{
		return "vimeo";
	}
	if(str.search("youtube")>=0)
	{
		this._setYoutubeCheck();

		return "youtube";
	}
};

videoWrapper.prototype._parseQuery = function(str)
{
	var parts = str.split("&");
	var obj = {};
	var idYes = false;

	for(var i = 0; i < parts.length; i++)
	{
		var kv = parts[i].split("=");

		obj[kv[0]] = kv[1];

		if(kv[0]=="player_id")
		{
			this.id = kv[1];
			idYes = true;
		}
	}

	if(!idYes)
	{
		this.id = this._generateID();
	}

	this.element.attr("id",this.id);

	return obj;
};

videoWrapper.prototype._generateID = function()
{
	var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
    {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    this.src += "&player_id="+text;
    this.element.attr("src",this.src);

    return text;
}

videoWrapper.prototype._map = function(value, low1, high1, low2, high2)
{
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
};

videoWrapper.prototype._addListeners = function()
{
	var copy = this;

	if(this.source=="vimeo")
	{
		if (window.addEventListener)
		{
	        window.addEventListener('message', function(e){ copy._onMessageReceived(e) }, false);
	    }
	    else
	    {
	        window.attachEvent('onmessage', function(e){ copy._onMessageReceived(e) }, false);
	    }
	}

	if(this.source=="html")
	{

		copy._setVideoInfo();
	}

    if(this.properties.seek_selector)
    {
    	$(this.properties.seek_selector).bind("mousedown",function(e)
    	{
    		e.preventDefault();

    		var wid = $(this).width(),
    			offsetX = e.offsetX;

    		var seconds = copy._map(offsetX,0,wid,0,copy.duration);

    		copy.seek(seconds);

    		return false;
    	});
    }
};

// HTML VIDEO HANDLING ================================================================

videoWrapper.prototype._setVideoInfo = function()
{
	var vid = this.element.get(0),
		copy = this;

	vid.addEventListener("canplay",function(e) {
		copy._setDefaults();
	}, false);

	vid.addEventListener("timeupdate",function(e) {
		var data = {};
		data.percent = vid.currentTime / vid.duration;

		copy._playing(data);
	}, false);

	vid.addEventListener("play",function(e) {
		copy.state = "play";
		copy._stateChange("play");
	}, false);

	vid.addEventListener("pause",function(e) {
		copy.state = "pause";
		copy._stateChange("pause");
	}, false);

	vid.addEventListener("ended",function(e) {
		copy._finish();
		if (typeof vid.loop != 'boolean')
		{
			if(copy.loopVar)
			{
				vid.seek(0);
			}
		}
	}, false);

	vid.addEventListener("seeked",function(e) {
		var data = {};
		data.percent = vid.currentTime / vid.duration;

		copy._seek(data);
	}, false);

	vid.addEventListener("progress",function(e) {
		var data = {};
		data.percent = vid.buffered.end(0) / vid.duration;

		copy._loading(data);
	}, false);
};

videoWrapper.prototype._setDefaults = function()
{
	var vid = this.element.get(0);

	this.duration = vid.duration;
	this.timeDuration = this.toTime(this.duration);
	this.volume = vid.volume;
	this.nativeWidth = vid.videoWidth;
	this.nativeHeight = vid.videoHeight;

	this._stateChange("ready");
};


// YOUTUBE HANDLING ================================================================

videoWrapper.prototype._setYoutubeCheck = function()
{
	var copy = this;

	var retry = function()
	{
		this.ytTimeout = setTimeout(function()
		{
			copy._setYoutubeCheck();
		},100);
	};

	if(typeof YT !== 'undefined')
	{
		if(YT.loaded)
		{
			this._setupYoutube();
		}
		else
		{
			retry();
		}
	}
	else
	{
		retry();
	}
};

videoWrapper.prototype._setupYoutube = function()
{
	var copy = this;

	this.ytPlayer = new YT.Player(this.id, {
		events: {
			'onReady': function(event) {
				copy._ytReady(event);
			}
		}
	});
};

videoWrapper.prototype._ytReady = function(event)
{
	var copy = this;

	this._onReady();
	this.ytPlayer.addEventListener('onStateChange', function(event){ copy._ytStateChange(event); });
};

videoWrapper.prototype._ytStateChange = function(event)
{
	switch(event.data)
	{
		case YT.PlayerState.ENDED:
			this.state = 'finish';
			if(this.loopVar)
			{
				this.seek(0);
			}
			this._finish();
			break;
		case YT.PlayerState.PLAYING:
			this.state = 'play';
			this._stateChange(this.state);
			this._ytSetPlaying(true);
			break;
		case YT.PlayerState.PAUSED:
			this.state = 'pause';
			this._stateChange(this.state);
			this._ytSetPlaying(false);
			break;
		case YT.PlayerState.BUFFERING: // unused

			break;
		case YT.PlayerState.CUED: // unused

			break;
	}
};

videoWrapper.prototype._ytSetPlaying = function(set)
{
	var copy = this;

	if(set)
	{
		this.ytTimeout = setTimeout(function()
		{
			var time = copy.ytPlayer.getCurrentTime();
			var perc = time / copy.duration;
			copy._playing({percent:perc});

			var loadtime = copy.ytPlayer.getVideoLoadedFraction();
			var perc = loadtime;
			copy._loading({percent:perc});

			copy._ytSetPlaying(true);
		},250);
	}
	else
	{
		clearTimeout(this.ytTimeout);
	}
};



// VIMEO HANDLING ================================================================

videoWrapper.prototype._onMessageReceived = function(e)
{
	var data = JSON.parse(e.data);

    
    if(data.player_id==this.id)
    {
	    if(data.method)
	    {
		    switch(data.method)
		    {
		        case 'getDuration':
		            this.duration = data.value;
		            this.timeDuration = this.toTime(this.duration);
		            break;
		        case 'getColor':
		            this.color = data.value;
		            break;
		        case 'getVideoWidth':
		            this.nativeWidth = data.value;
		            break;
		        case 'getVideoHeight':
		            this.nativeHeight = data.value;
		            break;
		        case 'getVideoUrl':
		            this.nativeUrl = data.value;
		            break;
		        case 'getVolume':
		            this.volume = data.value;
		            break;
		    }
	    }
	    else
	    {
		    switch(data.event)
		    {
		        case 'playProgress':
		            this._playing(data.data);
		            break;
		        case 'loadProgress':
		            this._loading(data.data);
		            break;
		        case 'seek':
		            this._seek(data.data);
		            break;
		        case 'ready':
		            this._onReady();
		            break;
		        case 'play':
		        case 'pause':
		        	this.state = data.event;
		            this._stateChange(data.event);
		            break;
		        case 'finish':
		        	this.state = data.event;
		            this._finish(data.event);
		            break;
		    }
	    }
	}
};

videoWrapper.prototype.post = function(action, value)
{
    var data = {
      method: action
    };
    
    if (value) {
        data.value = value;
    }
    
    var message = JSON.stringify(data);
    this.element.get(0).contentWindow.postMessage(data, this.url);
};




// API HANDLING ================================================================

videoWrapper.prototype._onReady = function()
{
    this.properties.ready();

    this.state = "ready";

    switch(this.source)
    {
    	case "vimeo":
			this.post('getDuration');
			this.post('getColor');
			this.post('getVideoHeight');
			this.post('getVideoWidth');
			this.post('getVideoUrl');
			this.post('getVolume');
		    this.post('addEventListener', 'play');
		    this.post('addEventListener', 'pause');
		    this.post('addEventListener', 'finish');
		    this.post('addEventListener', 'playProgress');
		    this.post('addEventListener', 'loadProgress');
		    this.post('addEventListener', 'seek');
		    break;
		case "youtube":
			this.duration 		= this.ytPlayer.getDuration();
		    this.timeDuration 	= this.toTime(this.duration);
		    this.nativeUrl 		= this.ytPlayer.getVideoUrl();
		    this.volume 		= this.ytPlayer.getVolume();
			break;
    }
};

videoWrapper.prototype._playing = function(data)
{
    this.progress.percentage = data.percent;

    var object = this.returnObject();
    this.properties.playing(object);
};

videoWrapper.prototype._loading = function(data)
{
    this.loadProgress.percentage = data.percent;

    var object = this.returnObject();
    this.properties.loading(object);
};

videoWrapper.prototype._seek = function(data)
{
    this.loadProgress.percentage = data.percent;

    var object = this.returnObject();
    this.properties.seek(object);
};

videoWrapper.prototype.returnObject = function()
{
	var obj = {};
	obj.progress 		= this.progress;
	obj.loadProgress 	= this.loadProgress;
	obj.url 			= this.url;
	obj.nativeUrl 		= this.nativeUrl;
	obj.duration		= this.duration;
	obj.timeDuration	= this.timeDuration;
	obj.colour 			= this.color;
	obj.state 			= this.state;

	switch(this.source)
	{
		case "vimeo":
			obj.volume 	= this.volume*100;
			break;
		case "youtube":
			obj.volume 	= this.volume;
			break;
		case "html":
			obj.volume 	= this.volume*100;
			break;
	}

	return obj;
}

videoWrapper.prototype.toTime = function(time)
{
    var sec_num = parseInt(time, 10);
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}

    if(hours>0)
    {
    	if (hours   < 10) {hours   = "0"+hours;}

    	var time    = hours+':'+minutes+':'+seconds;
    }
    else
    {
    	var time    = minutes+':'+seconds;
    }
    return time;
};

videoWrapper.prototype._stateChange = function(state)
{
    var object = this.returnObject();
	this.properties.change(object);
};
videoWrapper.prototype._finish = function()
{
    var object = this.returnObject();
	this.properties.finish(object);
};

videoWrapper.prototype.unload = function()
{
	switch(this.source)
	{
		case "youtube":
			this.ytPlayer.stopVideo();
			this.ytPlayer.clearVideo();
			break;
		case "vimeo":
			this.post("unload");
			break;
		case "html":
			this.seek(0);
			this.pause();
			break;
	}
};
videoWrapper.prototype.play = function()
{
	switch(this.source)
	{
		case "youtube":
			this.ytPlayer.playVideo();
			break;
		case "vimeo":
			this.post("play");
			break;
		case "html":
			this.element.get(0).play();
			break;
	}
};
videoWrapper.prototype.pause = function()
{
	switch(this.source)
	{
		case "youtube":
			this.ytPlayer.pauseVideo();
			break;
		case "vimeo":
			this.post("pause");
			break;
		case "html":
			this.element.get(0).pause();
			break;
	}
};
videoWrapper.prototype.seek = function(seconds)
{
	switch(this.source)
	{
		case "youtube":
			this.ytPlayer.seekTo(seconds,true);
			break;
		case "vimeo":
			this.post("seekTo",seconds);
			break;
		case "html":
			this.element.get(0).currentTime = seconds;
			break;
	}
};
videoWrapper.prototype.setVolume = function(vol)
{
	switch(this.source)
	{
		case "youtube":
			this.volume = vol;
			this.ytPlayer.setVolume(this.volume);
			break;
		case "vimeo":
			this.volume = (vol/100);
			this.post("setVolume",this.volume);
			break;
		case "html":
			this.volume = (vol/100);
			this.element.get(0).volume = this.volume;
			break;
	}
};
videoWrapper.prototype.loop = function(bool)
{
	this.loopVar = bool;

	switch(this.source)
	{
		case "youtube":

			break;
		case "vimeo":
			this.post("setLoop",bool);
			break;
		case "html":
			if (typeof this.element.get(0).loop == 'boolean')
			{
				this.element.get(0).loop = bool;
			}
			break;
	}
};
videoWrapper.prototype.colorChange = function(colour)
{
	switch(this.source)
	{
		case "youtube":
			console.log("The changing of colour is unsupported for youtube videos");
			break;
		case "vimeo":
			this.post("setColor",colour);
			this.post('getColor');
			break;
	}
};


var tag = document.createElement('script');
tag.src = "//www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
